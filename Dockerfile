FROM node:14-alpine3.17
RUN apk update && apk upgrade && \
  echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
  echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
  apk add --no-cache  \
  git@edge \
  gnutls@edge \ 
  tiff@edge \
  libcrypto3@edge \
  libssl3@edge \
  chromium@edge \
  && rm -rf /var/cache/apk/* \
  /tmp/*# Install Pa11y-Dashboard
RUN git clone https://github.com/pa11y/pa11y-dashboard.git pa11y-dashboard
WORKDIR /pa11y-dashboard
RUN npm install -g npm@latest
RUN CI= npm install --omit=dev 
RUN npm audit fix --force || true
COPY config/* config/
ENV NODE_ENV=production
ENV CHROME_BIN=/usr/bin/chromium-browser
RUN npm install json-schema@0.4.0
ENTRYPOINT ["node", "index.js"]
