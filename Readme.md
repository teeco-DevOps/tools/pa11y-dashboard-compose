# Pa11y Dashboard Docker Deployment
This repository provides a Docker version of the Pa11y Dashboard, an open-source web interface for the Pa11y accessibility testing tool. It allows you to easily deploy and manage the Pa11y Dashboard using Docker and Docker Compose.

## Prerequisites
Before proceeding with the deployment, ensure that you have the following prerequisites installed on your system:

- Docker: [Install Docker](https://docs.docker.com/get-docker/)
- Docker Compose: [Install Docker Compose](https://docs.docker.com/compose/install/)

## Setup Instructions
Follow these steps to set up the Pa11y Dashboard using Docker and Docker Compose:

### Clone the repository:

```bash
git clone https://gitlab.com/teeco-DevOps/tools/pa11y-dashboard-compose.git
```
### Navigate to the project directory:

```bash
cd pa11y-dashboard-docker
```
### Create a .env file and set the required environment variables:

```dosini
DATABASE_DIR=/path/to/database/directory
DB_USERNAME=your_db_username
DB_PASSWORD=your_db_password
```
Replace /path/to/database/directory with the absolute path to the directory where you want to store the MongoDB data.

### Modify the docker-compose.yml file if necessary:

If you need to customize any Docker-related settings, such as exposed ports or volume mappings, you can modify the docker-compose.yml file to suit your requirements.

### Start the Pa11y Dashboard containers:

```bash
docker-compose --build
docker-compose up -d 
```
This command will build the necessary Docker images and start the Pa11y Dashboard containers in the background.

### Access the Pa11y Dashboard:

Open your web browser and navigate to http://localhost:8080 to access the Pa11y Dashboard.

## Start running Pa11y tests:

Once the Pa11y Dashboard is set up, you can start running Pa11y tests by following the instructions provided in the [Pa11y Dashboard documentation](https://github.com/pa11y/pa11y-dashboard).

## Notes
The provided config/production.js contains configuration to run the dashboards inside alpine docker container. Modify the file as needed by referring to the [documentation](https://github.com/pa11y/pa11y-dashboard).


That's it! You've successfully set up the Pa11y Dashboard using Docker and Docker Compose. You can now use the Pa11y Dashboard web interface to monitor accessibility issues on your website and track improvements over time.